import { Component } from '@angular/core';

import { ExplorePage } from '../explore/explore';
import { NewMoviePage } from '../new-movie/new-movie';
import { UserPage } from '../user/user';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ExplorePage;
  tab2Root = NewMoviePage;
  tab3Root = UserPage;

  constructor() {

  }
}
