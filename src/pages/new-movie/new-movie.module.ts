import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewMoviePage } from './new-movie';

@NgModule({
  declarations: [
    NewMoviePage,
  ],
  imports: [
    IonicPageModule.forChild(NewMoviePage),
  ],
})
export class NewMoviePageModule {}
