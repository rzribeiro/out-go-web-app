import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Movie } from '../../models/movie';

import { MovieProvider } from '../../providers/movie/movie';

/**
 * Generated class for the NewMoviePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-movie',
  templateUrl: 'new-movie.html',
})
export class NewMoviePage {

  public movie: Movie;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public movieProvider: MovieProvider,
              public toastCtrl : ToastController) {
    this.movie = new Movie();
  }

  ionViewDidLoad() {
  }

  saveMovie(){
    this.movieProvider.create(this.movie).then((id) => {
      console.log(id);
      this.showToastOK('middle');
      this.movie = new Movie();
    }).catch((error) => {
      console.log(error);
      this.showToastError('middle');
    });
  }

  onImageFileLoad(base64Image: any){
    this.movie.image = base64Image;
  }

  showToastOK(position: string) {
    let toast = this.toastCtrl.create({
      message: 'Filme inserido com sucesso!',
      duration: 2000,
      position: position
    });

    toast.present(toast);
  }

  showToastError(position: string) {
    let toast = this.toastCtrl.create({
      message: 'Erro ao inserir filme, tente novamente.',
      duration: 2000,
      position: position
    });

    toast.present(toast);
  }

}
