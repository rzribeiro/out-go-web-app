import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoryProvider } from '../../providers/category/category';

/**
 * Generated class for the ExplorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-explore',
  templateUrl: 'explore.html',
})
export class ExplorePage {

  public categories: any;

  public nameSearch: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public categoryProvider: CategoryProvider) {
  }

  ionViewDidEnter() {
   this.categoryProvider.getAll().then((data) => {
      this.categories = data;
   });
  }

  showSearchResult(){
    return !(this.nameSearch == undefined || this.nameSearch == '');
  }

  showCategories(){
    return this.nameSearch == undefined || this.nameSearch == '';
  }

}
