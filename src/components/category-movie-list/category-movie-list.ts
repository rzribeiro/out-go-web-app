import { Component, Input } from '@angular/core';
import { Category } from '../../models/category';
import { Movie } from '../../models/movie';
import { MovieProvider } from '../../providers/movie/movie';
import { CategoryProvider } from '../../providers/category/category';

/**
 * Generated class for the CategoryMovieListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'category-movie-list',
  templateUrl: 'category-movie-list.html'
})
export class CategoryMovieListComponent {
  private _category: Category;

  private _nameSearch: string;

  public movies: Array<Movie>;

  public categories: Array<Category>;

  public sectionName: string;

  @Input() showCategoryBadget: Boolean;

  constructor(public movieProvider: MovieProvider, public categoryProvider: CategoryProvider) {
    categoryProvider.getAll().then((categories) => {
        this.categories = <Array<Category>> categories;
    });
  }

  @Input()
  set category(category: Category){
    this._category = category
    this.sectionName = category.name;
    this.movieProvider.getByCategory(category.id)
                      .then((movies) =>{
                        this.movies = <Array<Movie>> movies;
                      });
  }

  get category(){
    return this._category;
  }

  @Input()
  set nameSearch(nameSearch: string){
    this._nameSearch = nameSearch;
    this.sectionName = "Resultado da pesquisa";
    this.movieProvider.getByName(this._nameSearch)
                      .then((movies) =>{
                        this.movies = <Array<Movie>> movies;
                      });
  }

  getCategoryName(movie: Movie){
    let result = this.categories.find(category => category.id == movie.category);
    if(result == undefined){
      return "";
    }
    return result.name;
  }



}
