import { NgModule } from '@angular/core';
import { CategoryMovieListComponent } from './category-movie-list/category-movie-list';
import { FileUploaderComponent } from './file-uploader/file-uploader';
@NgModule({
	declarations: [CategoryMovieListComponent,
    FileUploaderComponent],
	imports: [],
	exports: [CategoryMovieListComponent,
    FileUploaderComponent]
})
export class ComponentsModule {}
