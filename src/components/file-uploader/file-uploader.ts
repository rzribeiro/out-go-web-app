import { Component, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the FileUploaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'file-uploader',
  templateUrl: 'file-uploader.html'
})
export class FileUploaderComponent {

  @Output() base64File = new EventEmitter<any>();

  constructor() {
  }

  onChangeInput(e){
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

    if(!this.isValidImageFile(file)){
      return
    }

    this.toBase64(file);

  }

  toBase64(file){
    var reader = new FileReader();
    reader.readAsDataURL(file);
    
    reader.onload =  () => {
      this.base64File.emit(reader.result);
    };
    reader.onerror = function (error) {
      return error;
    };
  }

  isValidImageFile(file){
    var pattern = /image-*/;

    if(file == undefined){
      return false;
    }

    if (!file.type.match(pattern)) {
      alert('Formato inválido!');
      return false;
    }

    return true;
  }
}
