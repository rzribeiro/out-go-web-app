import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Movie } from '../../models/movie';

/*
  Generated class for the MovieProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieProvider {
  private API_URL = 'http://localhost:3000/api/v1/movies';

  constructor(public http: HttpClient) {
  }

  create(movie: Movie){
    return new Promise((resolve, reject) => {
      this.http.post(this.API_URL, movie).subscribe((result: any) => {
        resolve(<Movie> result);
      },
      (error) => {
        reject(error);
      });
    });
  }

  getByCategory(category: number){
    return new Promise((resolve, reject) => {
      this.http.get(this.API_URL+'?category='+category+'&name='+name).subscribe((result: any) => {
        resolve(result);
      },
      (error) => {
        reject(error);
      });
    });
  }

  getByName(name: string){
    return new Promise((resolve, reject) => {
      this.http.get(this.API_URL+'?name='+name).subscribe((result: any) => {
        resolve(result);
      },
      (error) => {
        reject(error);
      });
    });
  }
}
