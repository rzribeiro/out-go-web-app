import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ExplorePage } from '../pages/explore/explore';
import { NewMoviePage } from '../pages/new-movie/new-movie';
import { UserPage } from '../pages/user/user';
import { MovieProvider } from '../providers/movie/movie';
import { CategoryProvider } from '../providers/category/category';
import { HttpClientModule } from '@angular/common/http';
import { CategoryMovieListComponent } from '../components/category-movie-list/category-movie-list';
import { FileUploaderComponent } from '../components/file-uploader/file-uploader';

@NgModule({
  declarations: [
    MyApp,
    ExplorePage,
    NewMoviePage,
    UserPage,
    CategoryMovieListComponent,
    FileUploaderComponent,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ExplorePage,
    NewMoviePage,
    UserPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MovieProvider,
    CategoryProvider
  ]
})
export class AppModule {}
