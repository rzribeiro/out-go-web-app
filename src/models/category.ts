import { Movie } from "./movie";

export class Category{
    public id: number;
    public name: string;
    public movies: Array<Movie>;
}