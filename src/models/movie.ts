export class Movie{
    public id: number;
    public title: string;
    public duration: number;
    public category: number;
    public categoryName: string;
    public image: Blob;
}